import logo from './logo.svg';
import './App.css';
import React from 'react'
import CreateBlog from './component/createBlog';
import DisplayBlog from './component/displayBlog';
import {Container,Row,Col} from 'reactstrap'
function App() {
  // const [blgData , setBlgData] =React.useState([])
  const [selectedMode , setSelectedMode] = React.useState([])
  // function passData(data){
  //   setBlgData(data)
  // }
  return (
    <div>
      <Container>
        <Row className="text-center mt-4">
          <h4>amit's Blog</h4>
          <hr />
          <Col>
          <h5>Create Blog</h5>
            <CreateBlog 
            // inputData={passData}
            selectedMode={selectedMode}
             />
          </Col>
          <Col>
          <h5>Display Information</h5>
            <DisplayBlog 
            // newHandler={passData}
            setSelectedMode={setSelectedMode}
            />
          </Col>
        </Row>
      </Container> 
    </div>
  );
}

export default App;
