import React, {useState} from 'react'
import {connect} from 'react-redux'
import { addBlog } from '../redux/action'
 function CreateBlog(props) {
    const [newState , setNewState]=useState(false)
    const [blogstate , setBlogstate] =useState([]);
    const [blog_name , setBlog_name] =useState("")
    const [blogtype , setBlogtype] =useState("");
    
    const b_name=(e)=>{
        setBlog_name(e.target.value)
    }
    const b_type=(e)=>{
        setBlogtype(e.target.value)
    }
    const createBlog=(e)=>{
        e.preventDefault()
       let blogname=e.target.blogname.value
       let type=e.target.type.value
       if(blogname=='' || type==''){
           alert("please insert all the fields")
       }
       else{
        setBlogstate({blogname:blogname ,type:type})   
        props.dispatch(addBlog({blogname:blogname ,type:type}))
        e.target.blogname.value=''
        e.target.type.value=''
        }
    }
    React.useEffect(() => {

    }, [props.selectedMode])
    return (
        <div>
           <form onSubmit={createBlog}>
               <input type="text" placeholder="Enter Blogname" value={blog_name} onChange={b_name} className="form-control mt-4" name="blogname" />
               <input type="text" placeholder="Enter Blog Type" value={blogtype} onChange={b_type} className="form-control mt-4" name="type" />
               <button className="btn btn-primary mt-4" style={{width:"50%"}}>submit</button>
           </form>
        </div>
    )
}
export default connect() (CreateBlog);