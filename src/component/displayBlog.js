import React ,{useEffect} from 'react'
import {useSelector, useDispatch } from 'react-redux';
import blogs from '../redux/reducer/reducers';
import {Button} from 'reactstrap'
import {deleteBlog} from '../redux/action/index'
import {connect} from 'react-redux'
function DisplayBlog(props,{setSelectedMode}) {
    const dispatch1 = useDispatch()
    console.log(dispatch1)
    // console.log(setSelectedMode)
    const selector = useSelector(state => state)
    // console.log(props)
    // const [blogdata , setBlogdata] =React.useState([])
    const updateValue=(e)=>{
        props.setSelectedMode(e)
        // props.newHandler(e)
    }
    useEffect(() => {
      props.fetchuser()
    }, [])
    // React.useEffect(() => {
    //  updateValue()
    // }, [updateValue()])
    return (
        <div>
            <table class="table">
                <tr>
                    <th>Blog Name</th>
                    <th>Blog Type</th>
                    <th>Delete</th>
                    <th>Update</th>
                </tr>
                {
                    selector.blogs.data.map((e,index)=>(
                        <tr key={index}>
                       <td>{e.data.blogname}</td> 
                        <td>{e.data.type}</td>
                        <td><Button color="danger" outline onClick={()=>{props.dispatch({type:'ADD_DELETE',id:e.id});console.log(e);}}>Delete</Button></td>
                        <td><Button color="warning" outline onClick={ ()=>updateValue(e)}>Update</Button></td> 
                        </tr>
                    ))
                }
            </table>
        </div>
    )
}

const mapStateToProps=(state)=>{
    return{
        userData:state.user
    }
}
const mapDispacthToProps=(dispatch)=>{
    return{
        fetchuser:()=>dispatch(fetch())
    }
}

export default connect(mapStateToProps,mapDispacthToProps) (DisplayBlog);