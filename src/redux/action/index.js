import axios from 'axios'
const ADD_BLOG = 'ADD_BLOG'
const DELETE_BLOG = 'DELETE_BLOG'
const UPDATE_BLOG = 'UPDATE_BLOG'
const SUCCESS ='SUCCESS'
const ERROR = 'ERROR'
const FETCH = 'FETCH'


export const addBlog = (message) => ({
    type: ADD_BLOG,
    message,
    id: Math.random()
})
export const deleteBlog = (id) => ({
    type: DELETE_BLOG,
    id,
})
export const updateBlog = (edit) => ({
    type: UPDATE_BLOG,
    edit,
})

export const fetch=(fetch)=>{
    return{
        type:FETCH,
        fetch
    }
}

export const success=(succ)=>{
    return{
        type:SUCCESS,
        succ
    }
}

export const error=(err)=>{
    return{
        type:ERROR,
        err
    }
}

export const user_fetch = (data) => {
    return (dispatch)=>{
        dispatch(fetch)
        axios.get("https://jsonplaceholder.typicode.com/todos")
        .then(response=>{
            dispatch(success(response))
        })
        .catch(err=>{
            console.log(err)
            dispatch(error(err))
        })

        
    }
}
