import {combineReducers} from 'redux'
import blogs from './reducers'

const rootReducer=combineReducers({
    blogs ,
})
export default rootReducer;