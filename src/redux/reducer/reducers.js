const initialState = {
    data: [],
    users: [],
    error: '',
    loading: false
}

const blogs = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_BLOG':
            return {
                ...state,
                data: [...state.data, { data: action.message, id: action.id }]
            }
            break;
        case 'ADD_DELETE':
            const blogs = state.data.filter((e) => (e.id !== action.id))
            return {
                ...state,
                data: blogs
            }
            break;
        case 'UPDATE_BLOG':
            return {
                ...state,
                data: [...state.data, { data: action.edit }]
            }
        case 'FETCH':
            return {
                ...state,
                loading: true,
            }
        case 'SUCCESS':
            return {
                loading: false,
                users: action.succ
            }
        case 'ERROR':
            return {
                loading: true,
                error: 'something went wrong'
            }
        default:
            return state
    }
}
export default blogs